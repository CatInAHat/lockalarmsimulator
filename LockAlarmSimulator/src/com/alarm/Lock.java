package com.alarm;

public class Lock implements AlarmListener{
	
	private String pin;
	
	public Lock(String pin){
		this.pin = pin;
	}
	
	@Override
	public void alarmTurnedOn(EnteredPinEvent e) {
		System.out.println(pin + " niewłasciwy. Alarm uruchomiony" + e.getAlarm().getPin() );
	}
	
	@Override
	public void alarmTurnedOff(EnteredPinEvent e) {
		System.out.println(pin + " właściwy. Alarm nieuruchomiony" + e.getAlarm().getPin() );
	}
}
