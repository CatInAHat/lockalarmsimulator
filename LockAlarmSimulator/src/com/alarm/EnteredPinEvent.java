package com.alarm;

public class EnteredPinEvent {

		private Alarm alarm;
		
		public EnteredPinEvent(Alarm alarm){
			this.alarm = alarm;			
		}

		public Alarm getAlarm(){
			return alarm;
		}
}
