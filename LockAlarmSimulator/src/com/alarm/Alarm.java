package com.alarm;

import java.util.List;
import java.util.ArrayList;

public class Alarm {

		private String pin;
		private List<AlarmListener> listeners;
		
		public void enterPin(String pin){
			
			Boolean isPinCorrect = this.pin == pin;
			
			if (isPinCorrect){
				this.correctEnteredPin();
			}
			else {
				this.wrongEnteredPin();
			}
		}
		
		public Alarm(String pin){
			
				this.pin = pin;
				this.listeners = new ArrayList<AlarmListener>();
		}
		
		public String getPin(){
			
			return pin;
		}
		
		public void addListener(AlarmListener listener){
			
			listeners.add(listener);
		}
		
		public void removeListener(AlarmListener listener){
			
			listeners.remove(listener);
		}
		
		private void wrongEnteredPin(){
			
			EnteredPinEvent e = new EnteredPinEvent(this);			
			for (AlarmListener l: listeners){
				l.alarmTurnedOn(e);
			}
		}
		
		private void correctEnteredPin(){
			
			EnteredPinEvent e = new EnteredPinEvent(this);
			for (AlarmListener l: listeners){
				l.alarmTurnedOff(e);
			}
		}
}
